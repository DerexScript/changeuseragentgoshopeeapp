document.addEventListener('DOMContentLoaded', async evt => {
    let btnSave = document.querySelector("#save");

    // Get already saved value of header JSON & set it in UI text area.
    chrome.storage.sync.get('headerUAContent', function (items) {
        if (typeof items.headerUAContent != "undefined") {
            document.querySelector("#header_input").value = items.headerUAContent;
        }
    });

    // Get already saved value of enable checkbox & check/uncheck UI checkbox with savd value.
    chrome.storage.sync.get('canChangeUA', function (items) {
        if (typeof items.canChangeUA != "undefined") {
            document.querySelector("#canChangeUA").checked = items.canChangeUA;
        }
    });

    btnSave.addEventListener("click", evt1 => {
        evt1.preventDefault();
        let canChangeUA = document.querySelector("#canChangeUA").checked;
        if (canChangeUA) {
            let headerUAContent = document.querySelector("#header_input").value;
            chrome.storage.sync.set({ 'canChangeUA': canChangeUA }, function () {
                console.log('Value is set to ' + canChangeUA);
            });
            chrome.storage.sync.set({ 'headerUAContent': headerUAContent }, function () {
                console.log('Value is set to ' + headerUAContent);
            });
            window.close();
        } else {
            console.log("You need to activate the plugin!");
        }
    });
}, false);
