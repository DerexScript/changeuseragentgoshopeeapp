'use strict';
chrome.runtime.onInstalled.addListener(async function () {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: { urlMatches: 'goshopee\.app' },
            })],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

chrome.webRequest.onBeforeSendHeaders.addListener(function (details) {
    let canChangeUA = true;
    let headerUAContent = `Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30`;
    chrome.storage.sync.get('canChangeUA', function (items) {
        if (typeof items.canChangeUA != "undefined") {
            canChangeUA = items.canChangeUA;
        }
    });
    chrome.storage.sync.get('headerUAContent', function (items) {
        if (typeof items.headerUAContent != "undefined") {
            headerUAContent = items.headerUAContent;
        }
    });
    if (canChangeUA) {
        for (var i = 0; i < details.requestHeaders.length; ++i) {
            if (details.requestHeaders[i].name === 'User-Agent') {
                details.requestHeaders[i].value = headerUAContent;
                break;
            }
        }
        return { requestHeaders: details.requestHeaders };
    }
}, { urls: ["https://*.goshopee.app/*"] }, ["blocking", "requestHeaders"]);

/*
chrome.webRequest.onBeforeSendHeaders.addListener(function (details) {
    for (var i = 0; i < details.requestHeaders.length; ++i) {
        if (details.requestHeaders[i].name === 'User-Agent') {
            details.requestHeaders[i].value = `Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30`;
            break;
        }
    }
    return { requestHeaders: details.requestHeaders };
}, { urls: ["https://*.goshopee.app/*"] }, ["blocking", "requestHeaders"]);
*/

/*
chrome.webRequest.onBeforeSendHeaders.addListener(function (info) {
    info.requestHeaders.forEach(function (header, i) {
        console.log("header - ");
        if (header.name.toLowerCase() == "user-agent".toLowerCase()) {
            header.value = `Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30`;
        }
    });
    return { requestHeaders: info.requestHeaders };
}, { urls: ["https://*.goshopee.app/*"] }, ["blocking", "requestHeaders"]);
*/

/*
chrome.webRequest.onSendHeaders.addListener(async function (details) {
    for (var i = 0; i < details.requestHeaders.length; ++i) {
        if (details.requestHeaders[i].name === 'User-Agent') {
            details.requestHeaders[i].value = `Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30`;
            console.log(details);
            break;
        }
    }
    return { requestHeaders: details.requestHeaders };
}, { urls: ["https://*.goshopee.app/*"], types: ["main_frame", "sub_frame", "stylesheet", "script", "image", "font", "object", "xmlhttprequest", "ping", "csp_report", "media", "websocket", "other"] }, ["requestHeaders"]);
*/

/*
chrome.webRequest.onBeforeRequest.addListener(function (details) {
    return { redirectUrl: "https://google.com.br" };
},{ urls: ["https://*.goshopee.app/*"], types: ["main_frame", "sub_frame", "stylesheet", "script", "image", "font", "object", "xmlhttprequest", "ping", "csp_report", "media", "websocket", "other"] }, ["blocking"]);
*/